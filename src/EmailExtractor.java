import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class EmailExtractor {

	final String protocol = "http://www.";
	final String baseUrl = "pdn.ac.lk/";
	final String path = "";

	final int TIMEOUT = 0;
	final int MAX_LEVEL = 0;

	int initialSize, currentSize, index = 0;

	Set<String> linksSet = new LinkedHashSet<>();
	Set<String> extractedEmails = new HashSet<String>();

	Document doc;

	final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+",
			Pattern.CASE_INSENSITIVE);

	public static void main(String[] args) {

		EmailExtractor emailExtractor = new EmailExtractor();

		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				emailExtractor.printEmails();
			}
		});

		emailExtractor.begin();
	}

	private void begin() {
		int step = 0;
		String url = protocol + baseUrl + path;

		extractLinks(url);

		if (step >= MAX_LEVEL) {
			return;
		}

		do {
			initialSize = linksSet.size();

			List<String> linksList = new ArrayList<String>(linksSet);

			for (; index < linksList.size(); index++) {
				String link = linksList.get(index);
				extractLinks(link);
			}
			
			currentSize = linksSet.size();
			step++;
			
		} while (initialSize < currentSize && step < MAX_LEVEL);
	}

	private void extractLinks(String url) {
		System.out.println("Reading: " + url);
		try {

			doc = Jsoup.connect(url).timeout(TIMEOUT).get();
			Elements links = doc.select("a");

			List<String> href = links.eachAttr("abs:href");

			Iterator<String> it = href.iterator();

			while (it.hasNext()) {
				String link = it.next();
				if (link.contains(baseUrl)) {
					linksSet.add(link);
				}
			}
			extractEmail(doc);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void extractEmail(Document doc) {
		String text = doc.body().text();
		Matcher m = VALID_EMAIL_ADDRESS_REGEX.matcher(text);
		while (m.find()) {
			String email = m.group();
			extractedEmails.add(email);
			System.out.println("Email found: " + email);
		}
	}

	public void printEmails() {
		System.out.println(extractedEmails.size() + " Emails Found");
		System.out.println(extractedEmails);
	}

}
